import React from "react";
import CountUp from "react-countup";
import cx from "classnames";
import { Card, Typography, Grid } from "@material-ui/core";

import styles from "./Cards.module.css";

function Cards({ data: { confirmed, deaths, recovered, lastUpdate } }) {
  if (!confirmed) return "loading...";

  return (
    <div className={styles.container}>
      <Grid container spacing={3} justify="center">
        <Grid
          item
          component={Card}
          xs={12}
          md={3}
          className={cx(styles.card, styles.infected)}
        >
          <Typography color="textSecondary" gutterBottom>
            Infected
          </Typography>
          <Typography varaint="h5">
            <CountUp
              start={0}
              end={confirmed.value}
              duration={1.5}
              separator=","
            />
          </Typography>
          <Typography color="textSecondary">
            {new Date(lastUpdate).toDateString()}
          </Typography>
          <Typography varaint="body2">
            Number of active cases of covid 19
          </Typography>
        </Grid>
        <Grid
          item
          component={Card}
          xs={12}
          md={3}
          className={cx(styles.card, styles.deaths)}
        >
          <Typography color="textSecondary" gutterBottom>
            Deaths
          </Typography>
          <Typography varaint="h5">
            <CountUp
              start={0}
              end={deaths.value}
              duration={1.5}
              separator=","
            />
          </Typography>
          <Typography color="textSecondary">
            {new Date(lastUpdate).toDateString()}
          </Typography>
          <Typography varaint="body2">
            Number of deadths cases of covid 19
          </Typography>
        </Grid>
        <Grid
          item
          component={Card}
          xs={12}
          md={3}
          className={cx(styles.card, styles.recovered)}
        >
          <Typography color="textSecondary" gutterBottom>
            Recovered
          </Typography>
          <Typography varaint="h5">
            <CountUp
              start={0}
              end={recovered.value}
              duration={1.5}
              separator=","
            />
          </Typography>
          <Typography color="textSecondary">
            {new Date(lastUpdate).toDateString()}
          </Typography>
          <Typography varaint="body2">
            Number of recovered cases of covid 19
          </Typography>
        </Grid>
      </Grid>
    </div>
  );
}

export default Cards;
